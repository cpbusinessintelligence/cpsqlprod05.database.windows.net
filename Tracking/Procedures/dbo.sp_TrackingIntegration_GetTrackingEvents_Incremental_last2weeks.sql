SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[sp_TrackingIntegration_GetTrackingEvents_Incremental_last2weeks]

AS
BEGIN
/*
Change History
-----------------------------------------------------------------------------
CH #153 - Anand - 12/06/2019 - Added condition when Workflowtype is Transfer and populated the AttributeValue when it is a DLB
CH #158 - Anand - 18/06/2019 - Changed when the outcome = onboard from in-transit to onboard
								Added NowGoConsignmentID and NowGoSubjectArticleID columns to be inserted into tbl_Tracking_Integration_Staging
CH #168 - Anand - 21/06/2019 - Changed ATL with Photo message to just display as ATL
CH #184 - Anand - 24/07/2019 - Changed Display text - Card left-in vehicle TO Card Left - In Vehicle AND Card left-unsafe to leave TO	Card Left - Unsafe to Leave
								Changed AttributeValues when workflowtype is PICKUP and outcome is failed-goods-not-ready OR failed-no-goods-to-go 
								failed-unable-to-transport OR failed-closed OR failed-no-access-available OR failed-pickup

*/



  SET NOCOUNT ON
DECLARE @Startdate  varchar(30)
DECLARE  @EndDDate  varchar(30)

SET @Startdate = '2019-09-10 00:00:00.000'
SET @EndDDate = '2019-09-10 23:59:59.999'

  SELECT DISTINCT * FROM 
  (
		-- Non Delivery Tracking Events
		SELECT 
			EventID,
			[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			CASE 	
			-- Changed from in-transit to onbard on 18/06/2019. will probably need to revisit once NowGo is released to other
				WHEN Outcome = 'onboard' THEN 'onboard'
				--CH #184
				WHEN Outcome = 'failed-pickup' THEN 'pickup'
				WHEN Outcome = 'failed-no-access-available' THEN 'pickup'
				WHEN Outcome = 'failed-closed' THEN 'pickup'
				WHEN Outcome = 'failed-unable-to-transport' THEN 'pickup'
				WHEN Outcome = 'failed-no-goods-to-go' THEN 'pickup'
				WHEN Outcome = 'failed-goods-not-ready' THEN 'pickup'
				ELSE Outcome
				END 
			AS ScanEvent, 
			[DriverRunNumber],
			[Branch],
			CASE WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popshop' THEN Attribute + IIF(AttributeValue IS NULL,'', '. Barcode - ' + AttributeValue) 
				 WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popstation' THEN Attribute + IIF(AttributeValue IS NULL,'', '. Barcode - ' + AttributeValue) 

				 --CH #184
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-pickup' THEN AttributeValue
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-no-access-available' THEN AttributeValue 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-closed' THEN AttributeValue  			 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-unable-to-transport' THEN AttributeValue 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-no-goods-to-go' THEN AttributeValue 
				 WHEN WorkflowType = 'pickup' AND Outcome = 'failed-goods-not-ready' THEN AttributeValue  			 
				 
				 ELSE NULL END 
			AS ExceptionReason,
			NULL AS AlternateBarcode,
			NULL AS [RedeliveryCard],
			CASE WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popshop' THEN AttributeValue 
				 WHEN WorkflowType = 'offboard' AND Outcome = 'offboard-popstation' THEN AttributeValue 
				 --CH #153	
				 WHEN WorkflowType = 'transfer' AND Outcome = 'transfer-query-freight' THEN IIF (charindex('DR', AttributeValue)>0,SUBSTRING(AttributeValue,3,20) , AttributeValue) 
				 WHEN WorkflowType = 'transfer' AND Outcome = 'transfer-other' THEN IIF (charindex('DR', AttributeValue)>0,SUBSTRING(AttributeValue,3,20) , AttributeValue) 
				 WHEN WorkflowType = 'transfer' AND Outcome = 'transfer-driver' THEN IIF (charindex('DR', AttributeValue)>0,SUBSTRING(AttributeValue,3,20) , AttributeValue)  			 
				 			 
				 ELSE NULL END 
			AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			NULL AS PODImageID,
			NULL AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID
		FROM [dbo].[tbl_TrackingEvent] WITH (NOLOCK)
		WHERE [WorkflowType] != 'deliver' AND [Outcome] != 'delivered'
		AND IsProcessed = 1
		AND CreatedDateTime >=  convert(datetime,@Startdate,121)
		AND CreatedDateTime <=  convert(datetime,@EndDDate,121)


		UNION

		-- Failed Delivery Tracking Events
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			CASE 
				 WHEN Outcome = 'failed-not-home-or-closed' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-other' THEN 'Attempted Delivery' 
				 WHEN Outcome = 'failed-incomplete' THEN 'Attempted Delivery' 
				 WHEN Outcome = 'failed-unsafe-to-leave' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-refused-by-customer' THEN 'Attempted Delivery'
				 WHEN Outcome = 'failed-damaged' THEN 'Attempted Delivery'				
				 ELSE Outcome
			END AS ScanEvent, 
			[DriverRunNumber],
			[Branch],
			CASE WHEN Outcome = 'failed-not-home-or-closed' THEN 'Card left-card left no response' 
				 WHEN Outcome = 'failed-unsafe-to-leave' THEN 'Card Left - Unsafe to Leave'
				 --CH#184
				 WHEN Outcome = 'failed-incomplete' THEN 'Card Left - In Vehicle'
				 WHEN Outcome = 'failed-other' THEN 'Card Left - In Vehicle'
				 WHEN Outcome = 'failed-refused-by-customer' THEN 'Card left-refused delivery'
				 WHEN Outcome = 'failed-damaged' THEN 'Card left-return to depot' 
				 ELSE NULL END 
			AS [ExceptionReason],
			NULL AS AlternateBarcode,
			IIF(Outcome = 'failed-not-home-or-closed' OR Outcome = 'failed-unsafe-to-leave', [AttributeValue], NULL) AS [RedeliveryCard],
			NULL AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			NULL AS PODImageID,
			NULL AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)		
		WHERE
		WorkflowType = 'deliver' AND [Outcome] != 'delivered'
		AND IsProcessed = 1
		AND CreatedDateTime >=  convert(datetime,@Startdate,121)
		AND CreatedDateTime <=  convert(datetime,@EndDDate,121)

		UNION

		-- Delivered Tracking Events with POD Signature
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			Outcome AS ScanEvent, 
			[DriverRunNumber],
			[Branch],
			NULL AS [ExceptionReason],
			NULL AS AlternateBarcode,
			NULL AS [RedeliveryCard],
			NULL AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			PODImageID,
			ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)
		INNER JOIN [tbl_TrackingEvent_PODImage] IMG WITH (NOLOCK)
		ON TRK.EventID = IMG.EventID
		WHERE
		[WorkflowType] = 'deliver' AND [Outcome] = 'delivered' AND IMG.IsDownloaded = 1 AND IMG.ImageDetail LIKE 'Signature%'
		AND IsProcessed = 1
		AND TRK.CreatedDateTime >=  convert(datetime,@Startdate,121)
		AND TRK.CreatedDateTime <=  convert(datetime,@EndDDate,121)


		UNION

		-- Delivered Tracking Events Without POD Image
		SELECT
			TRK.EventID,
			TRK.[Labelnumber] AS [TrackingNumber], 
			NULL AS [ConsignmentNumber],
			[WorkflowType],
			[Outcome],
			CONVERT(VARCHAR, EventDatetime, 120) AS [EventDateTime], 
			Outcome AS ScanEvent, 
			[DriverRunNumber],
			[Branch],
			--CH # 168 - Change ATL Display Message
			CASE WHEN Attribute LIKE 'ATL%' THEN 'ATL'
				 WHEN Attribute LIKE 'DLB%' THEN 'Delivery Barcode - ' + AttributeValue			 
				 ELSE NULL END 
			AS [ExceptionReason],
			NULL AS AlternateBarcode,
			NULL AS [RedeliveryCard],
			IIF([Attribute] = 'DLB', [AttributeValue], NULL) AS [DLB],
			NULL AS [URL],
			[SigneeName] AS [PODName],
			NULL AS PODImageID,
			NULL AS ImageType,
			NowGoWorkflowCompletionID,
			NowGoConsignmentID,
			NowGoSubjectArticleID
		FROM [dbo].[tbl_TrackingEvent] TRK WITH (NOLOCK)
		INNER JOIN [tbl_TrackingEvent_PODImage] IMG WITH (NOLOCK)
		ON TRK.EventID = IMG.EventID
		WHERE
		[WorkflowType] = 'deliver' AND [Outcome] = 'delivered' AND (IMG.NowGoImageID IS NULL OR IMG.ImageDetail NOT LIKE 'Signature%')
		AND IsProcessed = 1
		AND TRK.CreatedDateTime >=  convert(datetime,@Startdate,121)
		AND TRK.CreatedDateTime <=  convert(datetime,@EndDDate,121)

		) A
END
GO
