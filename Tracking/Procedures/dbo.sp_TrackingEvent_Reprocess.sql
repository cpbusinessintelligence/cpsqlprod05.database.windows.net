SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROC dbo.sp_TrackingEvent_Reprocess

AS

BEGIN
--select * FROM [dbo].[tbl_TrackingEvent_Reprocess]
--select * from [dbo].[tbl_TrackingEvent_Failed] with(nolock)
--truncate table [dbo].[tbl_TrackingEvent_Failed]
--exec dbo.sp_TrackingEvent_Reprocess

DECLARE @JSONString varchar(max),@RawID int

DECLARE jsonstring_cursor CURSOR  
    FOR SELECT rawid,jsonstring FROM [dbo].[tbl_TrackingEvent_Reprocess]  
OPEN jsonstring_cursor  
FETCH NEXT FROM jsonstring_cursor INTO @RawId,@JSONString

WHILE @@FETCH_STATUS = 0  
BEGIN

EXECUTE [dbo].[sp_TrackingEvent_Insert_Failed] @JSONString

UPDATE [dbo].[tbl_TrackingEvent_Reprocess] set IsProcessed = 1 where RawId = @RawId

FETCH NEXT FROM jsonstring_cursor INTO @RawId,@JSONString
END

CLOSE jsonstring_cursor;  
DEALLOCATE jsonstring_cursor; 

END


GO
