SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_TrackingEvent_ProntoImport_Staging_Insert]
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRAN

	BEGIN TRY
		
		SELECT
			[TrackingEventID]
			,[WorkflowType]
			,CAST(CAST([LabelNumber] AS BIGINT) AS VARCHAR) AS [LabelNumber] -- CAST TO REMOVE LEADING ZERO
			,[dbo].[fn_GET_COUPON_TYPE](LEFT(CAST(CAST([LabelNumber] AS BIGINT) AS VARCHAR),3)) AS [CouponType] -- CAST TO REMOVE LEADING ZERO
			,[EventDateTime]
			,[DriverRef]
			,[NowGoWorkflowCompletionID]
		INTO #temp_TrackingEvent_ProntoImport
		FROM [dbo].[tbl_TrackingEvent_ProntoImport]  WITH (NOLOCK) 	
		WHERE
			(LABELNUMBER LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
			OR
			LABELNUMBER LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') -- For coupons with leading zero
			AND 
			[IsProcessedCPNImport] = 0

		-- INSERT METRO AND LINK COUPON RECORDS
		INSERT INTO [dbo].[tbl_TrackingEvent_ProntoImport_Staging]
           ([TrackingEventID]
           ,[RecordType]
           ,[Contractor]
           ,[DateTimeStamp]
           ,[CouponSerialNumber]
           ,[LinkSerialNumber]
           ,[ReturnsSerialNumber]
           ,[AccountCode]
           ,[CustomerPhoneNumber]
           ,[InvoiceNumber]
           ,[TransactionType]
           ,[ConsignmentType]
           ,[InsuranceCode]
           ,[InsuranceValue]
           ,[LocationCode])
		SELECT 
			 T1.[TrackingEventID]
			,'S' AS [RecordType]
			,T1.[DriverRef] AS [Contractor]
			,T1.[EventDateTime] AS [DateTimeStamp]
			,T1.[LabelNumber] AS [CouponSerialNumber]
			,T2.[LabelNumber] AS [LinkSerialNumber]
			,NULL AS [ReturnsSerialNumber]
			,NULL AS [AccountCode]
			,NULL AS [CustomerPhoneNumber]
			,NULL AS [InvoiceNumber]
			,CASE WHEN T1.[WorkflowType] = 'pickup' THEN 'P' ELSE 'D' END AS  [TransactionType]
			,'CP' AS [ConsignmentType]
			,NULL AS [InsuranceCode]
			,NULL AS [InsuranceValue]
			,NULL AS [LocationCode]
			FROM
		(SELECT * FROM #temp_TrackingEvent_ProntoImport  WITH (NOLOCK) 	 WHERE CouponType = 'METRO') AS T1
		LEFT JOIN
		(SELECT * FROM #temp_TrackingEvent_ProntoImport  WITH (NOLOCK) 	WHERE CouponType = 'LINK') AS T2
		ON T1.NowGoWorkflowCompletionID = T2.NowGoWorkflowCompletionID


		-- INSERT RETURN COUPON RECORDS
		INSERT INTO [dbo].[tbl_TrackingEvent_ProntoImport_Staging]
           ([TrackingEventID]
           ,[RecordType]
           ,[Contractor]
           ,[DateTimeStamp]
           ,[CouponSerialNumber]
           ,[LinkSerialNumber]
           ,[ReturnsSerialNumber]
           ,[AccountCode]
           ,[CustomerPhoneNumber]
           ,[InvoiceNumber]
           ,[TransactionType]
           ,[ConsignmentType]
           ,[InsuranceCode]
           ,[InsuranceValue]
           ,[LocationCode])
		SELECT 
			 T1.[TrackingEventID]
			,'S' AS [RecordType]
			,T1.[DriverRef] AS [Contractor]
			,T1.[EventDateTime] AS [DateTimeStamp]
			,T2.[LabelNumber] AS [CouponSerialNumber]
			,NULL AS [LinkSerialNumber]
			,T1.[LabelNumber] AS [ReturnsSerialNumber]
			,NULL AS [AccountCode]
			,NULL AS [CustomerPhoneNumber]
			,NULL AS [InvoiceNumber]
			,CASE WHEN T1.[WorkflowType] = 'pickup' THEN 'P' ELSE 'D' END AS  [TransactionType]
			,'RP' AS [ConsignmentType]
			,NULL AS [InsuranceCode]
			,NULL AS [InsuranceValue]
			,NULL AS [LocationCode]
			FROM
		(SELECT * FROM #temp_TrackingEvent_ProntoImport  WITH (NOLOCK)  WHERE CouponType = 'RETURN') AS T1
		LEFT JOIN
		(SELECT * FROM #temp_TrackingEvent_ProntoImport  WITH (NOLOCK) 	WHERE CouponType = 'METRO') AS T2		
		ON T1.NowGoWorkflowCompletionID = T2.NowGoWorkflowCompletionID

	END TRY

	BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		
				ROLLBACK TRAN;
		END;

		THROW;

	END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
	END
END
GO
