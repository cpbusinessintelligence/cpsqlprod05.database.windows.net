SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sp_TrackingIntegration_GetTrackingEventsJSON_Incremental]
(
	@Lower INT,
	@Upper INT
)
AS
BEGIN

    SET NOCOUNT ON

	SELECT (
		SELECT 
			DISTINCT
				[TrackingNumber]
				,[ConsignmentNumber]
				,[EventDateTime]
				,[ScanEvent]
				,[DriverRunNumber]
				,[Branch]
				,[ExceptionReason]
				,ISNULL((SELECT 
							[TrackingNumber] AS Barcode
						FROM [tbl_TrackingEvent_Integration_Staging_Incremental] TRK2 WITH (NOLOCK) 
						WHERE 
							TRK1.[NowGoWorkflowCompletionID] = TRK2.[NowGoWorkflowCompletionID] and TRK1.[NowGoConsignmentId] = TRK2.[NowGoConsignmentId] and trk1.[NowGoSubjectArticleId] =  trk2.[NowGoSubjectArticleId]
							AND [TrackingNumber] LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' 
							AND TRK2.[TrackingNumber] != TRK1.[TrackingNumber]
							AND TRK1.[WorkflowType] IN ('pickup','deliver')
						FOR JSON PATH, INCLUDE_NULL_VALUES),
						JSON_QUERY('[]')) AS 'AlternateBarcode'
				,[RedeliveryCard]
				,[DLB]
				,[URL]
				,[PODName]
				,[PODImageURL]
		FROM [tbl_TrackingEvent_Integration_Staging_Incremental] TRK1
		WHERE  [RowId] BETWEEN @Lower AND @Upper
		FOR JSON PATH, INCLUDE_NULL_VALUES
	) AS JSONString
END
GO
