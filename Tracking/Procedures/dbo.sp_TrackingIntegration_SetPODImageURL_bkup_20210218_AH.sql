SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_TrackingIntegration_SetPODImageURL_bkup_20210218_AH]
(
	@BlobFolderEnpoint VARCHAR(100),
	@BlobFolderSharedAccessSignature VARCHAR(200)
)
AS
BEGIN
    SET NOCOUNT ON

	UPDATE [tbl_TrackingEvent_Integration_Staging]
	SET [PODImageURL] = CASE WHEN ImageType IS NOT NULL THEN @BlobFolderEnpoint + CONVERT(VARCHAR, PODImageID) + [dbo].[fn_GET_IMAGE_EXTN] (ImageType) + @BlobFolderSharedAccessSignature ELSE NULL END;	
	
	 
END
GO
