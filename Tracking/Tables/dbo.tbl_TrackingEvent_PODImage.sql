SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_PODImage] (
		[PODImageID]          [int] IDENTITY(1, 1) NOT NULL,
		[EventID]             [int] NOT NULL,
		[LabelNumber]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NowGoImageID]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ImageType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HasContents]         [bit] NULL,
		[ImageDetail]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDownloaded]        [bit] NULL,
		[DownloadMessage]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_PODImage]
		PRIMARY KEY
		CLUSTERED
		([PODImageID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_PODImage]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_PODImage_IsDownloaded]
	DEFAULT ((0)) FOR [IsDownloaded]
GO
CREATE NONCLUSTERED INDEX [nci_wi_tbl_TrackingEvent_PODImage_06D948CEA1AC95D69B1959CFB7624AC4]
	ON [dbo].[tbl_TrackingEvent_PODImage] ([EventID], [IsDownloaded], [ImageDetail])
	INCLUDE ([ImageType], [NowGoImageID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_tbl_TrackingEvent_PODImage_57D6A100B334D91C6807F3A4DDBE9796]
	ON [dbo].[tbl_TrackingEvent_PODImage] ([IsDownloaded], [ImageDetail])
	INCLUDE ([EventID], [ImageType])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_PODImage] SET (LOCK_ESCALATION = TABLE)
GO
