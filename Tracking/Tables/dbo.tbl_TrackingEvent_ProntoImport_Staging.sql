SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Staging] (
		[RecordID]                [int] IDENTITY(1, 1) NOT NULL,
		[TrackingEventID]         [int] NULL,
		[RecordType]              [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Contractor]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DateTimeStamp]           [datetime] NULL,
		[CouponSerialNumber]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LinkSerialNumber]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReturnsSerialNumber]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountCode]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerPhoneNumber]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InvoiceNumber]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TransactionType]         [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentType]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InsuranceCode]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InsuranceValue]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LocationCode]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_ProntoImport_Staging]
		PRIMARY KEY
		CLUSTERED
		([RecordID])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Staging] SET (LOCK_ESCALATION = TABLE)
GO
