SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_WebHooks_AccessData] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[Message_Id]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Access_Token]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[IsProcessed]         [bit] NULL,
		[ProcessedDate]       [datetime] NULL,
		CONSTRAINT [PK_tbl_WebHooks_AccessData]
		PRIMARY KEY
		CLUSTERED
		([Message_Id], [CreatedDateTime])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_WebHooks_AccessData]
	ADD
	CONSTRAINT [DF_tbl_WebHooks_AccessData_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_WebHooks_AccessData] SET (LOCK_ESCALATION = TABLE)
GO
