SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_trackingevent_agent_delete] (
		[TrackingNumber]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventDateTime]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ScanEvent]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverRunNumber]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExceptionReason]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AlternateBarcode]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedeliveryCard]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DLB]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PODName]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PODImageURL]           [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]           [bit] NULL,
		[CreatedDate]           [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_trackingevent_agent_delete]
	ADD
	CONSTRAINT [DF__tbl_track__IsPro__7A3223E8]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tbl_trackingevent_agent_delete]
	ADD
	CONSTRAINT [DF__tbl_track__Creat__7B264821]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tbl_trackingevent_agent_delete] SET (LOCK_ESCALATION = TABLE)
GO
