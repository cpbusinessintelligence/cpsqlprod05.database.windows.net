SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempBEPODLoad_PV] (
		[LabelNumber]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PODImageID]          [int] NOT NULL,
		[Attribute]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AttributeValue]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverId]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverExtRef]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverRunNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeviceId]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventDateTime]       [datetime] NOT NULL,
		[SigneeName]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDate]         [datetime] NULL,
		[IsLoadedToDWH]       [int] NULL
)
GO
ALTER TABLE [dbo].[tbl_TempBEPODLoad_PV] SET (LOCK_ESCALATION = TABLE)
GO
