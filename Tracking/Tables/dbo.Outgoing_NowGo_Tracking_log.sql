SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Outgoing_NowGo_Tracking_log] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[TrackingJSON]        [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		CONSTRAINT [Outgoing_NowGo_tracking]
		PRIMARY KEY
		CLUSTERED
		([RowID], [CreatedDateTime])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Outgoing_NowGo_Tracking_log]
	ADD
	CONSTRAINT [DF_Outgoing_NowGo_tracking_log_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[Outgoing_NowGo_Tracking_log] SET (LOCK_ESCALATION = TABLE)
GO
