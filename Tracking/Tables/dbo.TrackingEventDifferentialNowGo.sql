SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrackingEventDifferentialNowGo] (
		[Labelnumber]         [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Workflowtype]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventDatetime]       [datetime] NULL,
		[CreatedDatetime]     [datetime] NULL
)
GO
ALTER TABLE [dbo].[TrackingEventDifferentialNowGo] SET (LOCK_ESCALATION = TABLE)
GO
