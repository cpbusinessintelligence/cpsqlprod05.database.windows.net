SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_Reprocess_ErrorLog] (
		[RowID]                [bigint] IDENTITY(1, 1) NOT NULL,
		[ErrorData]            [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorDescription]     [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDateTime]       [datetime2](7) NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_Reprocess_ErrorLog]
		PRIMARY KEY
		CLUSTERED
		([RowID])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Reprocess_ErrorLog]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_Reprocess_ErrorLog_CreateDateTime]
	DEFAULT (getdate()) FOR [CreateDateTime]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Reprocess_ErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
