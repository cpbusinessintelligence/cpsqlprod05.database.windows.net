SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Incoming_NowGo_Tracking_log_Incremental] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[TrackingJSON]        [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		CONSTRAINT [PK_Incoming_NowGo_tracking_log_Incremental]
		PRIMARY KEY
		CLUSTERED
		([RowID], [CreatedDateTime])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_NowGo_Tracking_log_Incremental]
	ADD
	CONSTRAINT [DF_Incoming_NowGo_tracking_log_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[Incoming_NowGo_Tracking_log_Incremental] SET (LOCK_ESCALATION = TABLE)
GO
