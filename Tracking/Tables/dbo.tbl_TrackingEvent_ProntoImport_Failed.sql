SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Failed] (
		[TrackingEventID]               [int] IDENTITY(1, 1) NOT NULL,
		[WorkflowType]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Outcome]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Relation]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventDateTime]                 [datetime] NOT NULL,
		[DriverRef]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSuccessful]                  [bit] NULL,
		[NowGoWorkflowCompletionID]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoConsignmentID]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessedCPNImport]          [bit] NULL,
		[IsProcessedSLSImport]          [bit] NULL,
		[CreatedDateTime]               [datetime] NOT NULL,
		[UpdatedDateTime]               [datetime] NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_ProntoImport_Failed]
		PRIMARY KEY
		CLUSTERED
		([TrackingEventID])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Failed]
	ADD
	CONSTRAINT [DF__tbl_Track__IsPro__345EC57D]
	DEFAULT ((0)) FOR [IsProcessedCPNImport]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Failed]
	ADD
	CONSTRAINT [DF__tbl_Track__IsPro__3552E9B6]
	DEFAULT ((0)) FOR [IsProcessedSLSImport]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_ProntoImport_Failed] SET (LOCK_ESCALATION = TABLE)
GO
