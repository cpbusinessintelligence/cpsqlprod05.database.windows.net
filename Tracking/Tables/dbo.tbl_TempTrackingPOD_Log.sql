SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempTrackingPOD_Log] (
		[PODLogid]                [int] IDENTITY(1, 1) NOT NULL,
		[ImageID]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExecutionInstanceId]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExceptionMsg]            [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StepID]                  [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LogTimeStamp]            [datetime] NULL
)
GO
ALTER TABLE [dbo].[tbl_TempTrackingPOD_Log] SET (LOCK_ESCALATION = TABLE)
GO
