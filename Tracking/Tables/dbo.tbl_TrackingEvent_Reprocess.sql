SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_Reprocess] (
		[RawID]           [int] IDENTITY(1, 1) NOT NULL,
		[JSONString]      [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]     [bit] NOT NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_Reprocess]
		PRIMARY KEY
		CLUSTERED
		([RawID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Reprocess]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_Reprocess_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_Reprocess] SET (LOCK_ESCALATION = TABLE)
GO
