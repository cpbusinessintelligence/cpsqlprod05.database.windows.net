SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent] (
		[EventID]                       [int] IDENTITY(54756, 1) NOT NULL,
		[ConsignmentNumber]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[WorkflowType]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Outcome]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Attribute]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AttributeValue]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Relation]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemCount]                     [int] NULL,
		[DriverId]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverExtRef]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverRunNumber]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeviceId]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Latitude]                      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Longitude]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SigneeName]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSuccessful]                  [bit] NULL,
		[NowGoWorkflowCompletionID]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoConsignmentID]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoSubjectArticleID]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]                   [bit] NULL,
		[EventDateTime]                 [datetime] NOT NULL,
		[CreatedDateTime]               [datetime] NOT NULL,
		[RecipientIdType]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RecipientDateOfBirth]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RecipientAge]                  [int] NULL,
		[BookingID]                     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WebhookFlag]                   [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TrackingEvent]
		PRIMARY KEY
		NONCLUSTERED
		([EventID])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_WebhookFlag]
	DEFAULT ('N') FOR [WebhookFlag]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
CREATE NONCLUSTERED INDEX [nci_wi_tbl_TrackingEvent_0099B919BDAD21648315450222F7F837]
	ON [dbo].[tbl_TrackingEvent] ([LabelNumber])
	ON [partition_scheme_trackingEvent_by_month] ([EventDateTime])
GO
CREATE NONCLUSTERED INDEX [nci_wi_tbl_TrackingEvent_6FB264D27A1FF725328ABE3FECC8A6DD]
	ON [dbo].[tbl_TrackingEvent] ([IsProcessed], [Outcome], [WorkflowType])
	INCLUDE ([Attribute], [AttributeValue], [Branch], [DriverExtRef], [DriverRunNumber], [EventDateTime], [EventID], [LabelNumber], [NowGoConsignmentID], [NowGoSubjectArticleID], [NowGoWorkflowCompletionID], [SigneeName])
	ON [partition_scheme_trackingEvent_by_month] ([EventDateTime])
GO
ALTER TABLE [dbo].[tbl_TrackingEvent] SET (LOCK_ESCALATION = TABLE)
GO
