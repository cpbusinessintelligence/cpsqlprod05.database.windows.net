SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_JP_NowGoBilling] (
		[ConsignmentNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelNumber]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EventDatetime]         [datetime] NULL
)
GO
ALTER TABLE [dbo].[Temp_JP_NowGoBilling] SET (LOCK_ESCALATION = TABLE)
GO
