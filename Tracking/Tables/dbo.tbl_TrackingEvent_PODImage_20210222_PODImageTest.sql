SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_PODImage_20210222_PODImageTest] (
		[PODImageID]          [int] NOT NULL,
		[EventID]             [int] NOT NULL,
		[LabelNumber]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NowGoImageID]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ImageType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HasContents]         [bit] NULL,
		[ImageDetail]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDownloaded]        [bit] NULL,
		[DownloadMessage]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_PODImage_20210222_PODImageTest] SET (LOCK_ESCALATION = TABLE)
GO
