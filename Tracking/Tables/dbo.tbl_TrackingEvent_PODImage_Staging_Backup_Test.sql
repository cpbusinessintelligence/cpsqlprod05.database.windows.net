SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_PODImage_Staging_Backup_Test] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[PODImageID]          [int] NOT NULL,
		[IsDownloaded]        [bit] NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_TrackingEvent_PODImage_Staging_Backup_Test]
		PRIMARY KEY
		CLUSTERED
		([RowID])
)
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_PODImage_Staging_Backup_Test]
	ADD
	CONSTRAINT [DF_tbl_TrackingEvent_PODImage_Staging_Backup_Test_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tbl_TrackingEvent_PODImage_Staging_Backup_Test] SET (LOCK_ESCALATION = TABLE)
GO
